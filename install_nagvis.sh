#!/bin/bash
# script para instalar livestatus y nagvis

apt-get install -y librrd-dev libboost-dev libboost-all-dev &&
wget https://mathias-kettner.de/support/1.4.0p38/mk-livestatus-1.4.0p38.tar.gz &&
tar -zxvf mk-livestatus-1.4.0p38.tar.gz &&
cd mk-livestatus-1.4.0p38 &&
./configure --prefix=/opt/mk-livestatus --with-nagios4 && make && make install &&
sed -i 's/event_broker_options=-1/event_broker_options=-1\nbroker_module=\/opt\/mk-livestatus\/lib\/mk-livestatus\/livestatus.o \/opt\/nagios\/var\/rw\/live/g' /opt/nagios/etc/nagios.cfg &&
service nagios restart &&
apt-get -y install php-sqlite3 php-mbstring &&
wget http://www.nagvis.org/share/nagvis-1.9.11.tar.gz &&
tar -zxvf nagvis-1.9.11.tar.gz &&
cd nagvis-1.9.11 &&
./install.sh -n /opt/nagios -p /opt/nagvis -l unix:/opt/nagios/var/rw/live -u www-data -g www-data -w /etc/apache2/sites-enabled/ -W /nagvis -a y &&
mv /etc/httpd/conf.d/nagvis.conf /etc/apache2/sites-enabled/ &&
service apache2 restart &&
echo instalacion finalizada
