<#
.NOTES
    Author         : DMCI
    Version        : 1.0
    Creation Date  : 19/03/2019
    Last Update    : 19/03/2019

.SYNOPSIS 
    Este Script descarga e instala el softaware NSClient++ (Agente de Monitoreo)

.DESCRIPTION
    Este Script descarga e instala el softaware NSClient++ (Agente de Monitoreo), tambien descarga los scrpit personalizados para diferentes chequeos del software,
    configura la ip del Server Nagios y crea las reglas de excepción para el puerto 5666 en el FireWall de Windows.
    Debe ajecutar el script enviando como parametros:

        * La ip del Server Nagios (ejemplo 1). En caso de ser varias ip, estas deben ir separadas por una coma "," (ejemplo 2), este parametro es requerido.
        * La ip del servidor NSCA y su correspondiente password, estos parametros son opcionales (ejemplo 3). 

.EXAMPLE
    .\nsclient_install-nsclient.ps1 -ip_server_nagios "192.168.0.1"

.EXAMPLE
    .\nsclient_install-nsclient.ps1 -ip_server_nagios "192.168.0.1,192.168.0.2"

.EXAMPLE
    .\nsclient_install-nsclient.ps1 -ip_server_nagios "192.168.0.1,192.168.0.2" -ip_server_nsca "172.168.1.1" -psw_server_nsca "password"

#>

#Se definen los parametros para capturar los datos enviados desde la consola 
param (
    [Parameter(Mandatory=$true)]
    [string]$ip_server_nagios, 
    [string]$ip_server_nsca, 
    [string]$psw_server_nsca
)

# Se crea directorio temporal para almacenar el ejecutable del instalador
    New-Item -ItemType directory -Path C:\tmp
    $client = new-object System.Net.WebClient

# Se instancia TLS1.2 para la descarga
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

#Se descarga el ejecutable desde el repositorio y se guarda el en directorio antes creado
    echo "Descargando binario..."
    $client.DownloadFile("https://github.com/mickem/nscp/releases/download/0.5.2.35/NSCP-0.5.2.35-x64.msi","C:\tmp\NSCP-0.5.2.35-x64.msi")

#Se ejecuta el instalador de NSClient
    echo "Instalando NSClient..."
    & msiexec /qn /log log.txt /i c:\tmp\NSCP-0.5.2.35-x64.msi INSTALLLOCATION=C:\NSClient CONF_CHECKS=YES CONF_NRPE=YES NRPEMODE=SAFE ALLOWED_HOSTS="127.0.0.1" SampleConfig=YES 
    Start-Sleep -s 30
    echo "NSClient instalado correctamente`n"
    Start-Sleep -s 1

#Se detiene el servicio de nscp (NSClient)
    echo "Deteniendo servicio NSCP..."
    Set-ExecutionPolicy RemoteSigned
    Stop-Service nscp
    echo "Servicio NSCP detenido`n"
    Start-Sleep -s 1

#Se descargan archivos de configuracion y script de chequeo y se guardan en directorio del NSClient 
    $client = new-object System.Net.WebClient
    echo "Copiando checks a directorio C:\NSClient\scripts..."
    $client.DownloadFile("https://gitlab.com/heberthardila/nagios/raw/master/nsclient/config/nsclient.ini","C:\NSClient\nsclient.ini")
    $client.DownloadFile("https://gitlab.com/dmcico/nagios/raw/master/nsclient/checks/check_windows_updates.ps1","C:\NSClient\scripts\check_windows_updates.ps1")
    $client.DownloadFile("https://gitlab.com/dmcico/nagios/raw/master/nsclient/checks/check_updates.vbs","C:\NSClient\scripts\check_updates.vbs")
    $client.DownloadFile("https://gitlab.com/dmcico/nagios/raw/master/nsclient/checks/check_symantec_av.vbs","C:\NSClient\scripts\check_symantec_av.vbs")
    $client.DownloadFile("https://gitlab.com/dmcico/nagios/raw/master/nsclient/checks/check_windows_time.bat","C:\NSClient\scripts\check_windows_time.bat")
    $client.DownloadFile("https://gitlab.com/dmcico/nagios/raw/master/nsclient/checks/check_users_connect_audit.vbs","C:\NSClient\scripts\check_users_connect_audit.vbs")
    echo "Checks copiados correctamente`n"


#Se configura la ip del Server Nagios en el archivo nsclient.ini.
    (Get-Content C:\NSClient\nsclient.ini) | ForEach-Object {$_ -replace "allowed hosts = 127.0.0.1, <IP_SERVER_NAGIOS>", "allowed hosts = 127.0.0.1,$ip_server_nagios"} | Set-Content C:\NSClient\nsclient.ini

#Si la ip del servidor NSCA es proporcionada, esta es configurada en el archivo nsclient.ini.
    if($ip_server_nsca){     
            (Get-Content C:\NSClient\nsclient.ini) | ForEach-Object {$_ -replace "address= <IP_SERVER_NAGIOS>", "address= $ip_server_nsca"} | Set-Content C:\NSClient\nsclient.ini
    }

#Si el password del servidor NSCA es proporcionado, este es configurado en el archivo nsclient.ini.
    if($psw_server_nsca){     
            (Get-Content C:\NSClient\nsclient.ini) | ForEach-Object {$_ -replace "password_nsca= <PASSWORD>", "password_nsca= $psw_server_nsca"} | Set-Content C:\NSClient\nsclient.ini
    }

#Se crea la regla de entrada en el FireWall de Windows para el puerto 5666
    echo "Agregando excepciones en el FireWall`n"
    netsh advfirewall firewall add rule name="NSClient Port 5666" dir=in action=allow protocol=TCP localport=5666    
    echo "Excepciones agregadas correctamente`n"

#Se inicia el servicio de nscp (NSClient)
    echo "Iniciando Servicio NSCP...`n"
    Start-Service nscp
    echo "Servicio NSCP iniciado correctamente! :)"

#Final del Script
